import requests
from nose.tools import assert_equal


cerveceria = ''
cervecerias = ''
ids = ''
state ='California'
id_assert = 761
phone = '7077694495'
name = 'Lagunitas Brewing Co'
street = '1280 N McDowell Blvd'
param ='lagunitas'


response = requests.get("https://api.openbrewerydb.org/breweries/autocomplete?query="+param+"")
print("1er request: ")
print(response.json())
print("--------------------------")

for r in response.json():
    if r['name'] == name:
        ids = ids + r['id'] + ','

ids = ids[:-1]
ids = str.split(ids, ',')

for id in ids:
    response = requests.get("https://api.openbrewerydb.org/breweries/"+id+"")
    print("2do request: ")
    print(response.json())
    print("--------------------------")
    if response.json()['state'] == state:
        cerveceria = response.json()

print("Ultimo resultado: ")
print(cerveceria)

assert_equal(cerveceria['id'], id_assert)
assert_equal(cerveceria['street'], street)
assert_equal(cerveceria['name'], name)
assert_equal(cerveceria['phone'], phone)

print("Passed")