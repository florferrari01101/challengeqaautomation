from nose.tools import assert_equal, assert_true
from selenium import webdriver
from pages.page_index import PageIndex
from selenium.webdriver.chrome.options import Options as ChromeOptions
import os

marca='Vondom'
text ='Heladera'
string_en_titulo = 'Heladeras'
URL = 'www.fravega.com'
path = os.path.abspath(os.path.join(__file__, "../..")) + "/drivers/chromedriver"
options = ChromeOptions()
options.add_argument('start-maximized')
driver = webdriver.Chrome(chrome_options=options,
                          executable_path=path)

driver.implicitly_wait(10)
driver.get(URL)
driver.maximize_window()
PageIndex.click_on_textfield_buscador(driver)
PageIndex.set_value_on_buscador(driver, text)
PageIndex.click_on_search_button(driver)

PageIndex.click_on_link_filter_heladera(driver)

PageIndex.click_on_checkbox_filter_vandom(driver)

titulos_elementos = PageIndex.get_titles_elements_grilla(driver)
for t in titulos_elementos:
    assert_true(marca in t.text)

cantidad_elementos = PageIndex.get_number_of_elements_grilla(driver)
elementos_grilla = PageIndex.get_elements_grilla(driver)
assert_equal(cantidad_elementos, len(elementos_grilla))
titulo = driver.title
assert_true(string_en_titulo in titulo)
driver.quit()