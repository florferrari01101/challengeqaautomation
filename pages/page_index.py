from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


class PageIndex():

    def click_on_textfield_buscador(self):
        input_buscador = (By.CLASS, 'InputBar__SearchInput-t6v2m1-1 iJaFAt')
        input_buscador = WebDriverWait(self, 5).until(EC.element_to_be_clickable(input_buscador))
        input_buscador.click()

    def set_value_on_buscador(self, texto):
        input_buscador = (By.CLASS, 'InputBar__SearchInput-t6v2m1-1 iJaFAt')
        input_buscador = WebDriverWait(self, 5).until(EC.element_to_be_clickable(input_buscador))
        input_buscador = texto

    def click_on_search_button(self):
        search_button = (By.CLASS, 'InputBar__SearchButton-t6v2m1-2 jRChuZ')
        search_button = WebDriverWait(self, 5).until(EC.element_to_be_clickable(search_button))
        search_button.click()

    def click_on_link_filter_heladera(self):
        link_heladeras = (By.PARTIAL_LINK_TEXT, 'Heladeras (')
        link_heladeras = WebDriverWait(self, 5).until(EC.element_to_be_clickable(link_heladeras))
        link_heladeras.click()

    def click_on_checkbox_filter_vandom(self):
        filtro_marca_vondom = (By.PARTIAL_LINK_TEXT, 'Vondom (')
        filtro_marca_vondom = WebDriverWait(self, 5).until(EC.element_to_be_clickable(filtro_marca_vondom))
        filtro_marca_vondom.click()

    def get_number_of_elements_grilla(self):
        grilla = (By.NAME, 'ProductCard__Card-sc-1w5guu7-2 hlRWOw')
        grilla = WebDriverWait(self, 5).until(EC.element_to_be_clickable(grilla))
        return len(grilla)

    def get_elements_grilla(self):
        grilla = (By.NAME, 'ProductCard__Card-sc-1w5guu7-2 hlRWOw')
        grilla = WebDriverWait(self, 5).until(EC.element_to_be_clickable(grilla))
        return grilla

    def get_titles_elements_grilla(self):
        titles = (By.NAME, 'PieceTitle-sc-1eg7yvt-0 akEoc')
        titles = WebDriverWait(self, 5).until(EC.element_to_be_clickable(titles))
        return titles